package sk.piro.ui.layout.gridBuilder;

/**
 * 
 */

/**
 * Fill behavior of content when it is smaller than cell. Specified horizontal and vertical fill
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public enum Fill {
	/**
	 * No filling
	 */
	NONE(false, false),
	/**
	 * Horizontally fill cell
	 */
	HORIZONTAL(true, false),
	/**
	 * Vertically fill cell
	 */
	VERTICAL(false, true),
	/**
	 * Fill cell horizontally and vertically
	 */
	BOTH(true, true);

	private final boolean horizontal;
	private final boolean vertical;

	private Fill(boolean horizontal, boolean vertical) {
		this.horizontal = horizontal;
		this.vertical = vertical;
	}

	/**
	 * Returns true if vertical fill is used
	 * 
	 * @return true if vertical fill is used
	 */
	public boolean vertical() {
		return this.vertical;
	}

	/**
	 * Returns true if horizontal fill is used
	 * 
	 * @return true if horizontal fill is used
	 */
	public boolean horizontal() {
		return this.horizontal;
	}
}
