package sk.piro.ui.layout.gridBuilder;

import java.io.Serializable;

import sk.piro.ui.layout.gridBuilder.template.LayoutTemplate;

/**
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <GridType>
 *            type of grid
 * @param <ContentType>
 *            type of content
 */
public class GridBuilder<GridType, ContentType> implements Serializable, GridBuilderI<GridType, ContentType> {
	private final GridType grid;

	private LayoutTemplate layoutTemplate = null;
	private Position position = new Position(0, 0);

	/**
	 * Create grid builder using specified grid
	 * 
	 * @param grid
	 *            to build
	 */
	public GridBuilder(GridType grid) {
		super();
		this.grid = grid;
	}

	@Override
	public Position getPosition() {
		return position;
	}

	@Override
	public int getRow() {
		return getPosition().getRow();
	}

	@Override
	public int getColumn() {
		return getPosition().getCol();
	}

	@Override
	public GridType getGrid() {
		return grid;
	}

	@Override
	public GridBuilder<GridType, ContentType> setLayoutTemplate(LayoutTemplate layoutTemplate) {
		this.layoutTemplate = layoutTemplate;
		return this;
	}

	@Override
	public LayoutTemplate getLayoutTemplate() {
		return layoutTemplate;
	}

}
