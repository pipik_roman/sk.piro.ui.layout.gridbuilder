package sk.piro.ui.layout.gridBuilder;

import java.io.Serializable;

/**
 * Specify cell position
 * 
 * @author Roman Pipík, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class Position implements Serializable {

	/**
	 * Row position
	 */
	public final int row;
	/**
	 * Column position
	 */
	public final int col;

	/**
	 * Create grid position
	 * 
	 * @param row
	 *            row position
	 * @param col
	 *            column position
	 */
	public Position(int row, int col) {
		assert row >= 0 : "Row has to be positive";
		assert col >= 0 : "Column has to be positive";
		this.row = row;
		this.col = col;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}

	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return the col
	 */
	public int getCol() {
		return col;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof Position)) return false;
		Position other = (Position) obj;
		if (col != other.col) return false;
		if (row != other.row) return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + row + "x" + col + "]";
	}

}
