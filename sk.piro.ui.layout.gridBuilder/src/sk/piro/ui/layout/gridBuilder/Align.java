package sk.piro.ui.layout.gridBuilder;

/**
 * Defines alignment, content placement and others. Since implementations can differ we have to use out own orientation definitions
 * 
 * @author Roman Pipík, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public enum Align {
	/**
	 * Upper middle orientation
	 */
	NORTH(false, true, false),
	/**
	 * lower middle orientation
	 */
	SOUTH(false, false, true),
	/**
	 * right middle orientation
	 */
	EAST(false, false, false),
	/**
	 * left middle orientation
	 */
	WEST(false, false, false),
	/**
	 * upper left orientation
	 */
	NORTH_WEST(false, true, false),
	/**
	 * upper right orientation
	 */
	NORTH_EAST(false, true, false),
	/**
	 * lower left orientation
	 */
	SOUTH_WEST(false, false, true),
	/**
	 * lower right orientation
	 */
	SOUTH_EAST(false, false, true),
	/**
	 * center orientation
	 */
	CENTER(false, false, false),
	/**
	 * For Right-to-left locale this is same as {@link #EAST}, for left-to-right this is same as {@link #WEST}
	 */
	LINE_START(true, false, false),
	/**
	 * For Right-to-left locale this is same as {@link #WEST}, for left-to-right this is same as {@link #EAST}
	 */
	LINE_END(true, false, false),
	/**
	 * For Right-to-left locale this is same as {@link #NORTH_EAST}, for left-to-right this is same as {@link #NORTH_WEST}
	 */
	LINE_START_TOP(true, true, false),
	/**
	 * For Right-to-left locale this is same as {@link #NORTH_WEST}, for left-to-right this is same as {@link #NORTH_EAST}
	 */
	LINE_END_TOP(true, true, false),
	/**
	 * For Right-to-left locale this is same as {@link #SOUTH_EAST}, for left-to-right this is same as {@link #SOUTH_WEST}
	 */
	LINE_START_BOTTOM(true, false, true),
	/**
	 * For Right-to-left locale this is same as {@link #SOUTH_WEST}, for left-to-right this is same as {@link #SOUTH_EAST}
	 */
	LINE_END_BOTTOM(true, false, true);

	private final boolean relative;
	private final boolean top;
	private final boolean bottom;

	/**
	 * 
	 */
	private Align(boolean relative, boolean top, boolean bottom) {
		assert !(bottom && top) : "Orientation cannot be top and bottom at once";
		this.relative = relative;
		this.top = top;
		this.bottom = bottom;
	}

	/**
	 * Returns true if orientation is relative. Orientation is relative when it is locale dependent, like {@link #LINE_START} and {@link #LINE_END} orientations
	 * 
	 * @return true if orientation is relative.
	 */
	public boolean isRelative() {
		return this.relative;
	}

	/**
	 * Returns non relative version of orientation depending on rightToLeftOrientation. For non-relative orientations this returns same orientation.
	 * 
	 * @param rightToLeft
	 *            true if right to left layout is used, false if left to right orientation is used
	 * @return non relative version of orientation depending on rightToLeftOrientation
	 */
	public Align getNonRelative(boolean rightToLeft) {
		if (!this.isRelative()) {
			return this;
		}
		else if (rightToLeft) {
			switch (this) {
				case LINE_END:
					return WEST;
				case LINE_END_BOTTOM:
					return SOUTH_WEST;
				case LINE_END_TOP:
					return NORTH_WEST;
				case LINE_START:
					return EAST;
				case LINE_START_BOTTOM:
					return SOUTH_EAST;
				case LINE_START_TOP:
					return NORTH_EAST;
				default:
					throw new AssertionError("Invalid value of Orientation: " + this);
			}
		}
		else {
			switch (this) {
				case LINE_END:
					return EAST;
				case LINE_END_BOTTOM:
					return SOUTH_EAST;
				case LINE_END_TOP:
					return NORTH_EAST;
				case LINE_START:
					return WEST;
				case LINE_START_BOTTOM:
					return SOUTH_WEST;
				case LINE_START_TOP:
					return NORTH_WEST;
				default:
					throw new AssertionError("Invalid value of Orientation: " + this);
			}
		}
	}

	/**
	 * Returns true if orientation is on top
	 * 
	 * @return true if orientation is on top
	 */
	public boolean isTop() {
		return top;
	}

	/**
	 * Returns true if orientation is on bottom
	 * 
	 * @return true if orientation is on bottom
	 */
	public boolean isBottom() {
		return bottom;
	}

	/**
	 * Returns true if orientation is not top or bottom
	 * 
	 * @return true if orientation is not top or bottom
	 */
	public boolean isVerticalMiddle() {
		return (!top) && (!bottom);
	}

	/**
	 * Returns true if orientation is right. Orientation has to be non relative!
	 * 
	 * @return true if orientation is right
	 */
	public boolean isRight() {
		if (isRelative()) throw new IllegalStateException("Cannot decide if isRight when relative orientation is used!");
		switch (this) {
			case NORTH_EAST:
			case SOUTH_EAST:
			case EAST:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if orientation is left. Orientation has to be non relative!
	 * 
	 * @return true if orientation is left
	 */
	public boolean isLeft() {
		if (isRelative()) throw new IllegalStateException("Cannot decide if isLeft when relative orientation is used!");
		switch (this) {
			case NORTH_WEST:
			case SOUTH_WEST:
			case WEST:
				return true;
			default:
				return false;
		}
	}

	/**
	 * Returns true if orientation is horizontal middle.
	 * 
	 * @return true if orientation is horizontal middle
	 */
	public boolean isHorizontalMiddle() {
		switch (this) {
			case CENTER:
			case NORTH:
			case SOUTH:
				return true;
			default:
				return false;
		}
	}
}
