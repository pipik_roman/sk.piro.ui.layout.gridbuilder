package sk.piro.ui.layout.gridBuilder.template;

import sk.piro.core.Builder;
import sk.piro.ui.layout.gridBuilder.Fill;
import sk.piro.ui.layout.gridBuilder.Align;

/**
 * Builder for cell template. Used by {@link LayoutTemplateBuilder}
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CellTemplateBuilder implements Builder<CellTemplate> {
	private boolean space;
	private Integer width;
	private Fill fill;
	private Align align;

	/**
	 * Create cell template builder
	 */
	public CellTemplateBuilder() {
		//
	}

	/**
	 * @return the align
	 */
	public Align getAlign() {
		return align;
	}

	/**
	 * @return the fill
	 */
	public Fill getFill() {
		return fill;
	}

	/**
	 * @return the width
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @return the space
	 */
	public boolean isSpace() {
		return space;
	}

	/**
	 * @param space
	 *            the space to set
	 */
	public void setSpace(boolean space) {
		this.space = space;
	}

	/**
	 * @param align
	 *            the align to set
	 */
	public void setAlign(Align align) {
		this.align = align;
	}

	/**
	 * @param fill
	 *            the fill to set
	 */
	public void setFill(Fill fill) {
		this.fill = fill;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * Set values to default
	 */
	public void reset() {
		space = false;
		fill = null;
		align = null;
		width = null;
	}

	@Override
	public CellTemplate create() {
		return new CellTemplate(space, fill, align, width);
	}
}
