package sk.piro.ui.layout.gridBuilder.template;

import java.util.ArrayList;
import java.util.List;

import sk.piro.core.Builder;
import sk.piro.ui.layout.gridBuilder.Align;
import sk.piro.ui.layout.gridBuilder.Fill;

/**
 * Builder for {@link LayoutTemplate}
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class LayoutTemplateBuilder implements Builder<LayoutTemplate> {

	private final boolean relative;
	private final List<CellTemplate> templates = new ArrayList<CellTemplate>();
	private final CellTemplateBuilder cellTemplateBuilder = new CellTemplateBuilder();
	private int columns = -1;
	private Fill defaultFill = null;
	private Align defaultAlign = null;

	/**
	 * Create builder for layout template
	 * 
	 * @param relative
	 *            true if widths are relative
	 */
	public LayoutTemplateBuilder(boolean relative) {
		this.relative = relative;
	}

	/**
	 * Set maximum number of columns
	 * 
	 * @param columns
	 *            maximum number of columns. Can be zero or negative to match cell templates
	 */
	public void setColumns(int columns) {
		this.columns = columns;
	}

	/**
	 * Add space to layout template
	 * 
	 * @param width
	 *            width of space, or null for default width
	 * @return this builder
	 */
	public LayoutTemplateBuilder addSpace(Integer width) {
		cellTemplateBuilder.reset();
		cellTemplateBuilder.setWidth(width);
		return addCellTemplate(cellTemplateBuilder.create());
	}

	/**
	 * Add cell template to layout template. Default fill and align will be used
	 * 
	 * @param width
	 *            width of cell or null
	 * @return this builder
	 */
	public LayoutTemplateBuilder addCell(Integer width) {
		return this.addCell(null, null, width);
	}

	/**
	 * Add cell template to layout template
	 * 
	 * @param fill
	 *            fill for cell, or null for default fill
	 * @param align
	 *            align for cell or null for default align
	 * @param width
	 *            width of cell or null
	 * @return this builder
	 */
	public LayoutTemplateBuilder addCell(Fill fill, Align align, Integer width) {
		cellTemplateBuilder.reset();
		cellTemplateBuilder.setFill(fill == null ? getDefaultFill() : fill);
		cellTemplateBuilder.setAlign(align == null ? getDefaultAlign() : align);
		cellTemplateBuilder.setWidth(width);
		return addCellTemplate(cellTemplateBuilder.create());
	}

	/**
	 * Add cell template to layout template
	 * 
	 * @param cellTemplate
	 *            cell template to add
	 * @return this builder
	 */
	public LayoutTemplateBuilder addCellTemplate(CellTemplate cellTemplate) {
		this.templates.add(cellTemplate);
		return this;
	}

	/**
	 * @return the defaultAlign
	 */
	public Align getDefaultAlign() {
		return defaultAlign;
	}

	/**
	 * @return the defaultFill
	 */
	public Fill getDefaultFill() {
		return defaultFill;
	}

	/**
	 * @param defaultAlign
	 *            the defaultAlign to set
	 */
	public void setDefaultAlign(Align defaultAlign) {
		this.defaultAlign = defaultAlign;
	}

	/**
	 * @param defaultFill
	 *            the defaultFill to set
	 */
	public void setDefaultFill(Fill defaultFill) {
		this.defaultFill = defaultFill;
	}

	@Override
	public LayoutTemplate create() {
		int cols = columns;
		if (columns <= 0) {
			cols = templates.size();
		}

		return new LayoutTemplate(templates, relative, cols);
	}

}
