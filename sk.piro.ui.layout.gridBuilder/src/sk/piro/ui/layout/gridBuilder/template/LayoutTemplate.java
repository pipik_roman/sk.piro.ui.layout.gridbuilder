package sk.piro.ui.layout.gridBuilder.template;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Template for layout
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class LayoutTemplate implements Iterable<CellTemplate>, Serializable {

	/**
	 * Unmodifiable list of cell templates
	 */
	public final List<CellTemplate> cellTemplates;

	/**
	 * True if cell widths are relative
	 */
	public final boolean relative;

	/**
	 * Maximum number of columns. Number of columns matches cell templates or can be larger
	 */
	public final int columns;

	/**
	 * Create layout template
	 * 
	 * @param cellTemplates
	 *            cell templates. List is not modified, but is wrapped by unmodifiable list
	 * @param relative
	 *            true if cell templates use relative layout
	 * @param columns
	 *            maximum number of columns
	 */
	public LayoutTemplate(List<CellTemplate> cellTemplates, boolean relative, int columns) {
		super();
		assert cellTemplates.size() <= columns : "Number of columns must be same or larger than number of cell templates";
		this.cellTemplates = Collections.unmodifiableList(cellTemplates);
		this.relative = relative;
		this.columns = columns;
	}

	/**
	 * @return the cellTemplates
	 */
	public List<CellTemplate> getCellTemplates() {
		return cellTemplates;
	}

	/**
	 * @return the relative
	 */
	public boolean isRelative() {
		return relative;
	}

	@Override
	public Iterator<CellTemplate> iterator() {
		return cellTemplates.iterator();
	}

	/**
	 * Returns number of columns (spaces included)
	 * 
	 * @return number of columns (spaces included)
	 */
	public int getColumnCount() {
		return cellTemplates.size();
	}
}
