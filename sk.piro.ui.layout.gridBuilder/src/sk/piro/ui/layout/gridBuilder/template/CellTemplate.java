package sk.piro.ui.layout.gridBuilder.template;

import java.io.Serializable;

import sk.piro.ui.layout.gridBuilder.Align;
import sk.piro.ui.layout.gridBuilder.Fill;

/**
 * Template for one cell
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CellTemplate implements Serializable {
	/**
	 * True if cell should be space
	 */
	public final boolean space;
	/**
	 * Cell fill
	 */
	public final Fill fill;
	/**
	 * Cell alignment
	 */
	public final Align align;

	/**
	 * Relative or absolute size width of cell. Relative width is specified in percents. Can be null for not specified
	 */
	public final Integer width;

	/**
	 * @param space
	 *            true if cell should be space
	 * @param fill
	 *            cell fill, can be null
	 * @param align
	 *            cell alignment, can be null
	 * @param width
	 *            cell width, relative (in percents) or absolute, can be null
	 */
	public CellTemplate(boolean space, Fill fill, Align align, Integer width) {
		super();
		this.space = space;
		this.fill = fill;
		this.align = align;
		this.width = width;
	}

	/**
	 * @return the align
	 */
	public Align getAlign() {
		return align;
	}

	/**
	 * @return the fill
	 */
	public Fill getFill() {
		return fill;
	}

	/**
	 * @return the width of cell, or null for not specified
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @return the space
	 */
	public boolean isSpace() {
		return space;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((align == null) ? 0 : align.hashCode());
		result = prime * result + ((fill == null) ? 0 : fill.hashCode());
		result = prime * result + (space ? 1231 : 1237);
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof CellTemplate)) { return false; }
		CellTemplate other = (CellTemplate) obj;
		if (align != other.align) { return false; }
		if (fill != other.fill) { return false; }
		if (space != other.space) { return false; }
		if (width != other.width) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "CellTemplate [space=" + space + ", fill=" + fill + ", align=" + align + ", width=" + width + "]";
	}

}
