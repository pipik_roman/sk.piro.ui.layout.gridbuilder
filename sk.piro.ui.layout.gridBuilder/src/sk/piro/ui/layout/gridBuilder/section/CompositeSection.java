package sk.piro.ui.layout.gridBuilder.section;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import sk.piro.ui.layout.gridBuilder.Position;

/**
 * Composite section iterating over other sections to create complex sections
 * 
 * @author Roman Pipík, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CompositeSection extends Section {

	private final List<Section> sections = new ArrayList<Section>();

	/**
	 * Create composite grid section. Sections with same name are removed, only
	 * one of such sections will be added
	 * 
	 * @param name
	 *            name of section
	 * @param sections
	 *            sections to add. All sections has to be row and column
	 *            specific
	 */
	public CompositeSection(String name, Section ... sections) {
		super(name);
		assert sections.length == 0 : "No section to add";
		for (Section s : sections) {
			assert s != null : "Section cannot be null";
			this.sections.add(s);
		}
	}

	/**
	 * Create composite grid section
	 * 
	 * @param name
	 *            name of section
	 * @param sections
	 *            sections to add. All sections has to be row and column
	 *            specific
	 */
	public CompositeSection(String name, Iterable<Section> sections) {
		super(name);
		for (Section s : sections) {
			assert s != null : "Section cannot be null";
			this.sections.add(s);
		}
		assert !this.sections.isEmpty() : "No sections added";
	}

	/**
	 * Add section to this composite section. If section with same name already
	 * exists it is not added and order of sections is not changed!
	 * 
	 * @param section
	 *            section to add
	 * @return this section for method chaining
	 */
	public CompositeSection add(Section section) {
		assert section != null : "Section cannot be null";
		this.sections.add(section);
		return this;
	}

	@Override
	public Iterator<Position> iterator() {
		return new Iterator<Position>() {
			private final Iterator<Section> sectionsIterator = sections.iterator();
			private Iterator<Position> positionIterator = sectionsIterator.next().iterator();

			@Override
			public boolean hasNext() {
				return positionIterator.hasNext() || sectionsIterator.hasNext();
			}

			@Override
			public Position next() {
				if (positionIterator.hasNext()) {
					return positionIterator.next();
				}
				else if (sectionsIterator.hasNext()) {
					positionIterator = sectionsIterator.next().iterator();
					return positionIterator.next();
				}
				else {
					throw new NoSuchElementException();
				}
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public int getSize() {
		int size = 0;
		for (Section section : sections) {
			size += section.getSize();
		}
		return size;
	}

	@Override
	public String toString() {
		return "CompositeGridSection [name=" + name + ", sections=" + sections + "]";
	}

}
