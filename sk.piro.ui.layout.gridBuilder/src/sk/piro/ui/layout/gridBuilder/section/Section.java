package sk.piro.ui.layout.gridBuilder.section;

import java.io.Serializable;

import sk.piro.ui.layout.gridBuilder.Position;

/**
 * Represents collection of cell position in section and section name. Sections are compared by name
 * 
 * @author Roman Pipík, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public abstract class Section implements Iterable<Position>, Serializable {

	/**
	 * Name of section. Name cannot be changed!
	 */
	public final String name;

	/**
	 * Create section
	 * 
	 * @param name
	 *            name of section
	 */
	public Section(String name) {
		super();
		this.name = name;
	}

	/**
	 * Returns name of section
	 * 
	 * @return name of section
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns number of cells in section
	 * 
	 * @return number of cells in section
	 */
	public abstract int getSize();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof Section)) { return false; }
		Section other = (Section) obj;
		if (name == null) {
			if (other.name != null) { return false; }
		}
		else if (!name.equals(other.name)) { return false; }
		return true;
	}

	@Override
	public String toString() {
		return "Section [name=" + name + "]";
	}

}
