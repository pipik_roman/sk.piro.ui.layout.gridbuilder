package sk.piro.ui.layout.gridBuilder.section;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import sk.piro.ui.layout.gridBuilder.Position;

/**
 * Section with list of cells.
 * 
 * @author Roman Pipík, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 */
public class CellSection extends Section {

	private final Set<Position> positions = new LinkedHashSet<Position>();

	/**
	 * Create cell section
	 * 
	 * @param name
	 *            name of section
	 * @param cells
	 *            cells to be contained in section
	 */
	public CellSection(String name, Position ... cells) {
		super(name);
		for (Position p : cells) {
			assert p != null : "Position cannot be null";
			positions.add(p);
		}
	}

	/**
	 * Create cell section
	 * 
	 * @param name
	 *            name of section
	 * @param cells
	 *            cells to be contained in section. Same cells are removed to
	 *            exist only once
	 */
	public CellSection(String name, Iterable<Position> cells) {
		super(name);
		for (Position p : cells) {
			assert p != null : "Position cannot be null";
			positions.add(p);
		}
	}

	/**
	 * Add cell to section. If cell already exist in section it is not added and
	 * order of cells is not changed!
	 * 
	 * @param cell
	 *            cell to add
	 * @return this section for method chaining
	 */
	public CellSection add(Position cell) {
		assert cell != null : "Position cannot be null";
		this.positions.add(cell);
		return this;
	}

	/**
	 * Add cell to section. If cell already exists in section it is not added and order of cells is not changed!
	 * 
	 * @param row
	 *            row of cell
	 * @param col
	 *            column of cell
	 * @return this section for method chaining
	 */
	public CellSection add(int row, int col) {
		return this.add(new Position(row, col));
	}

	@Override
	public Iterator<Position> iterator() {
		return positions.iterator();
	}

	@Override
	public String toString() {
		return "CellSection [positions=" + positions + "]";
	}

	@Override
	public int getSize() {
		return positions.size();
	}
}
