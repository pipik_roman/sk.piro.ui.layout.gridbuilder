package sk.piro.ui.layout.gridBuilder;

import sk.piro.ui.layout.gridBuilder.template.LayoutTemplate;

/**
 * Interface to define grid builder
 * 
 * @author Roman Pipik, pipik.roman@gmail.com, roman.pipik@dominanz.sk
 * @param <GridType>
 *            grid type
 * @param <ContentType>
 *            content type
 */
public interface GridBuilderI<GridType, ContentType> {

	// --------TEMPLATE
	/**
	 * Set actual template
	 * 
	 * @param layoutTemplate
	 *            template to set, or null to use no template
	 * @return this builder
	 */
	public GridBuilderI<GridType, ContentType> setLayoutTemplate(LayoutTemplate layoutTemplate);

	/**
	 * Returns actual layout template
	 * 
	 * @return actual layout template
	 */
	public LayoutTemplate getLayoutTemplate();

	// ----------BASICS

	/**
	 * Returns actual row position
	 * 
	 * @return actual row position
	 */
	public int getRow();

	/**
	 * Returns actual column position
	 * 
	 * @return actual column position
	 */
	public int getColumn();

	/**
	 * Returns actual position
	 * 
	 * @return actual position
	 */
	public Position getPosition();

	/**
	 * Returns used grid object
	 * 
	 * @return used grid object
	 */
	public GridType getGrid();

	// -----------ACTIONS

	/**
	 * Moves position to new row
	 * 
	 * @return builder for method chaining
	 */
	public GridBuilderI<GridType, ContentType> newRow();

	/**
	 * Add column space
	 * 
	 * @return builder for method chaining
	 */
	public GridBuilderI<GridType, ContentType> space();

	/**
	 * Add row space. If columns is not 0 {@link #newRow()} is invoked first
	 * 
	 * @return builder for method chaining
	 */
	public GridBuilderI<GridType, ContentType> spaceRow();

	/**
	 * add multiple column spaces
	 * 
	 * @param spaces
	 *            number of spaces
	 * @return builder for method chaining
	 */
	public GridBuilderI<GridType, ContentType> space(int spaces);

	/**
	 * Set column span on actual position
	 * 
	 * @param cols
	 *            number of columns that actual position spans
	 * @return builder for method chaining
	 */
	public GridBuilderI<GridType, ContentType> span(int cols);

	// ------------
	/**
	 * Dynamic columns are pushed by spanning to right
	 * 
	 * @return true if columns have dynamic strategy
	 */
	public boolean isDynamic();

	/**
	 * Static columns are not pushed by spanning
	 * 
	 * @return true if columns have static strategy
	 */
	public boolean isStatic();
}
